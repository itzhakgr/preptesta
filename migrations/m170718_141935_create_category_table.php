<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category`.
 */
class m170718_141935_create_category_table extends Migration
{
    /**
     * @inheritdoc
     */
	public function up()
    {
        $this->createTable('category',
		[
            'id' => $this->integer(),
			'name' => $this->string(),
			'PRIMARY KEY(id)',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('category');
    }
}
