<?php

use yii\db\Migration;

/**
 * Handles the creation of table `activity`.
 */
class m170718_142322_create_activity_table extends Migration
{
    /**
     * @inheritdoc
     */
	public function up()
    {
        $this->createTable('activity',
		[
            'id' => $this->integer(),
			'title' => $this->string(),
			'categoryId' => $this->integer(),
			'statusId' => $this->integer(),
			'PRIMARY KEY(id)',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('activity');
    }
}
