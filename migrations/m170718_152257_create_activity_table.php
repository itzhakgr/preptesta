<?php

use yii\db\Migration;

/**
 * Handles the creation of table `activity`.
 */
class m170718_152257_create_activity_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('activity', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'categoryId' => $this->integer(),
            'statusId' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('activity');
    }
}
