<?php

use yii\db\Migration;

/**
 * Handles the creation of table `status`.
 */
class m170718_142626_create_status_table extends Migration
{
    /**
     * @inheritdoc
     */
	public function up()
    {
        $this->createTable('status',
		[
            'id' => $this->integer(),
			'name' => $this->string(),
			'PRIMARY KEY(id)',
        ]);
    }


    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('status');
    }
}
