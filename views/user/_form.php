<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Category;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <!--?= $form->field($model, 'id')->textInput() ?-->

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

		<!-- שאלה 3-->
	<?php if(!$model->isNewRecord){ ?>
			<?= $form->field($model, 'role')-> dropDownList(User::getRoles()) ?>
	<?php } ?>
	
	<?php if(!$model->isNewRecord){ ?>
			<?= $form->field($model, 'categoryId')-> dropDownList(Category::getCategories()) ?>
	<?php } ?>
	
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

	

	
    <?php ActiveForm::end(); ?>

</div>
